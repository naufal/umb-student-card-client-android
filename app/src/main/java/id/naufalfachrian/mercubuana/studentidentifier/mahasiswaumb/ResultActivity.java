package id.naufalfachrian.mercubuana.studentidentifier.mahasiswaumb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Profil Mahasiswa");
        StudentProfile profile = (StudentProfile) getIntent().getSerializableExtra("profile");
        TextView text_nim = (TextView) findViewById(R.id.text_nim);
        TextView text_nama = (TextView) findViewById(R.id.text_nama);
        TextView text_jenis_kelamin = (TextView) findViewById(R.id.text_jenis_kelamin);
        TextView text_program_studi = (TextView) findViewById(R.id.text_program_studi);
        TextView text_fakultas = (TextView) findViewById(R.id.text_fakultas);
        TextView text_semester_awal = (TextView) findViewById(R.id.text_semester_awal);
        text_nim.setText(profile.getNIM());
        text_nama.setText(profile.getNama());
        text_jenis_kelamin.setText(profile.getJenisKelamin());
        text_program_studi.setText(profile.getProgramStudi());
        text_fakultas.setText(profile.getFakultas());
        text_semester_awal.setText(profile.getSemesterAwal());
    }
}
