package id.naufalfachrian.mercubuana.studentidentifier.mahasiswaumb;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new CheckHostAvailability().execute();
    }

    class CheckHostAvailability extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            SocketAddress socketAddress = new InetSocketAddress("naufalfachrian.96.lt", 80);
            Socket socket = new Socket();
            int timeout_ms = 8000;
            try {
                socket.connect(socketAddress, timeout_ms);
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            onHostAvailable(result);
        }
    }

    private void onHostAvailable(boolean available) {
        if (available) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                Snackbar.make(findViewById(R.id.activity_main),
                        "Obviously, we need to use the camera on your phone.", Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                                onHostAvailable(true);
                            }
                        })
                        .show();
            } else {
                Intent scannerIntent = new Intent(this, ScannerActivity.class);
                startActivity(scannerIntent);
                finish();
            }
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Host Unreachable")
                    .setMessage("Please check your Internet connection or ask " + getString(R.string.author) + " for more information.")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
        }

    }
}
