package id.naufalfachrian.mercubuana.studentidentifier.mahasiswaumb;

import android.content.Context;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by littleflower on 11/12/16.
 */
public class RequestHandler {

    private RequestQueue Queue;

    public RequestHandler() {
    }

    private RequestQueue getRequestQueue(Context context) {
        if (Queue == null) {
            Queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return Queue;
    }

    public <T> void addToRequestQueue(Context context, Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(2000, 10, 0));
        getRequestQueue(context).add(req);
    }
}
