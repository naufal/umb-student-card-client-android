package id.naufalfachrian.mercubuana.studentidentifier.mahasiswaumb;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import org.json.JSONException;
import org.json.JSONObject;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);
        scannerView.setKeepScreenOn(true);
        setContentView(scannerView);
        setTitle(getString(R.string.scanner_activity_title));
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void handleResult(Result result) {
        progressDialog = ProgressDialog.show(this, null, "Please wait ...", true);
        if (result.getBarcodeFormat() == BarcodeFormat.CODE_39 && result.getText().length() == 11) {
            new RetrieveStudentProfile().get(result.getText());
        } else {
            showInvalidCardAlertDialog();
        }
    }

    private void showInvalidCardAlertDialog() {
        showAlertDialog("Invalid Student Card", "Please use a valid Mercu Buana Student's Identity Card. " +
                "If you think this is an application error, please tell " + getString(R.string.author) + ".");
    }

    private void showAlertDialog(String title, String message) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onResume();
                    }
                })
                .show();
    }

    private void showStudentProfile(StudentProfile profile) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        Intent studentIntent = new Intent(this, ResultActivity.class);
        studentIntent.putExtra("profile", profile);
        startActivity(studentIntent);
    }

    class RetrieveStudentProfile {
        public void get(String nim) {
            new RequestHandler().addToRequestQueue(getApplicationContext(),
                    new StringRequest(
                            Request.Method.GET,
                            "http://naufalfachrian.96.lt/api/mercubuana/student/nim/" + nim,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    JSONObject json_student_profile = null;
                                    try {
                                        json_student_profile = new JSONObject(response);
                                        StudentProfile studentProfile = new StudentProfile();
                                        studentProfile.setNIM(json_student_profile.getString("NIM"));
                                        studentProfile.setNama(json_student_profile.getString("Nama"));
                                        studentProfile.setJenisKelamin(json_student_profile.getString("Jenis Kelamin"));
                                        studentProfile.setFakultas(json_student_profile.getString("Fakultas"));
                                        studentProfile.setProgramStudi(json_student_profile.getString("Program Studi"));
                                        studentProfile.setSemesterAwal(json_student_profile.getString("Semester Awal"));
                                        showStudentProfile(studentProfile);
                                    } catch (JSONException e) {
                                        showAlertDialog("Fail to Extract JSON Object",
                                                "It seems there are API change on server side, please contact " + getString(R.string.author) + "immediately.");
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    NetworkResponse response = error.networkResponse;
                                    if (response != null) {
                                        switch (response.statusCode) {
                                            case 400:
                                            case 404:
                                                showInvalidCardAlertDialog();
                                                break;
                                            case 500:
                                                showAlertDialog("Server Error", "It seems the server was not working now.");
                                                break;
                                            default:
                                                showAlertDialog("Unidentified Error : " + response.statusCode, "Unidentified error occurred, please tell Naufal Fachrian about this.");
                                        }
                                    }
                                }
                            }
                    ));
        }
    }
}
