package id.naufalfachrian.mercubuana.studentidentifier.mahasiswaumb;

import java.io.Serializable;

/**
 * Created by littleflower on 11/12/16.
 */
public class StudentProfile implements Serializable {
    private String NIM;
    private String Nama;
    private String JenisKelamin;
    private String Fakultas;
    private String ProgramStudi;
    private String SemesterAwal;

    public String getNIM() {
        return NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getJenisKelamin() {
        return JenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        JenisKelamin = jenisKelamin;
    }

    public String getFakultas() {
        return Fakultas;
    }

    public void setFakultas(String fakultas) {
        Fakultas = fakultas;
    }

    public String getProgramStudi() {
        return ProgramStudi;
    }

    public void setProgramStudi(String programStudi) {
        ProgramStudi = programStudi;
    }

    public String getSemesterAwal() {
        return SemesterAwal;
    }

    public void setSemesterAwal(String semesterAwal) {
        SemesterAwal = semesterAwal;
    }
}
